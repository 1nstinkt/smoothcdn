var app = require("express")();

var multiparty = require("multiparty"),
    fs = require("fs"),
    crypto = require('crypto'),
    sharp = require('sharp'),
    avlFileTypes = ['avatar', 'gallery', 'portfolio_video'],
    maxSize = 1000 * 1024 * 1024,
    supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png', 'video/mp4'],
    corsOrigins = ['http://localhost:8090', 'http://localhost:3000', 'http://smooth.style', 'http://dev.smooth.style', 'http://smooth_web', 'http://api.smooth.style', 'http://api-dev.smooth.style', 'http://admin.smooth.style', 'http://adm-dev.smooth.style'],
    permittedIPAddresses = [
        '127.0.0.1',
        'localhost',
        '54.154.157.222' // api-dev
    ],
    uploadFolderPath = {
        avatar: 'images/',
        gallery: 'images/',
        portfolio_video: 'videos/'
    },
    PORT = 7070;

sharp.cache(false); // disable the libvips file cache

function checkPermission(req) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    for (var pip in permittedIPAddresses) {
        if (ip.indexOf(permittedIPAddresses[pip]) !== -1) return true;
    }

    console.error(ip+' tried to run secured operation.');
    return false;
}

app.post("/:file_type/:entity_id", function (req, res, next) {
    // CORS for noted hosts
    var origin = req.headers.origin;
    if (corsOrigins.indexOf(origin) !== -1) {
        res.set('Access-Control-Allow-Origin', origin);
    }

    var form = new multiparty.Form(),
        // array with errors, happened during upload
        errors = [],
        shell = require('shelljs');

    if (avlFileTypes.indexOf(req.params.file_type) == -1) {
        return res.status(400).send({
            status: 'Error',
            errors: ['"' + req.params.file_type + '" file type doesn\'t support.']
        })
    } else {
        var uploadFile = {uploadPath: uploadFolderPath[req.params.file_type], uploadFolder: '', type: '', size: 0, filename: '', to_update: true, path: '', path_resized: ''}
    }

    uploadFile.uploadFolder = uploadFile.uploadPath + req.params.entity_id + '/' + req.params.file_type;

    // if error occurred
    form.on('error', function(errors) {
        if (fs.existsSync(uploadFile.path)) {
            //если загружаемый файл существует удаляем его
            fs.unlinkSync(uploadFile.path);
            res.status(400).send({status: 'Error', errors: errors, filename: uploadFile.filename});
        }
    });

    form.on('close', function() {
        if (errors.length) {
            if (fs.existsSync(uploadFile.path)) {
                //если загружаемый файл существует удаляем его
                fs.unlinkSync(uploadFile.path);
            }
        }
    });

    // while file in coming
    form.on('part', function(part) {
        // read its size in bites
        uploadFile.size = part.byteCount;
        // read its type
        uploadFile.type = part.headers['content-type'];
        // store file name for later use
        uploadFile.filename = part.filename;
        // generate path for storing file
        uploadFile.path = uploadFile.uploadFolder + '/' + uploadFile.filename.split('/').pop();
        if (!fs.existsSync(uploadFile.path)) {
            uploadFile.to_update = false; // mark to save - not update
            // if file doesn't exists = create new, else - rewrite old
            uploadFile.path = uploadFile.uploadFolder + '/' + Date.now() + '_' + crypto.createHash('md5').update(part.filename).digest("hex") + '.' + uploadFile.filename.split('.').pop();
        }

        // check size of file, its shouldn't be maximum
        if (uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size + '. Limit is ' + (maxSize / 1024 / 1024) + ' MB.');
        }

        // check if type is supported
        if (supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        // if errors are absent - create write stream
        if (errors.length == 0) {
            if (req.params.file_type == 'avatar') {
                // only one avatar image for each user
                shell.rm('-rf', uploadFile.uploadFolder + '/*');
            }

            // if user folder doesn't exist - create it
            if (!fs.existsSync(uploadFile.uploadFolder)) {
                shell.mkdir('-p', uploadFile.uploadFolder);
            }

            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
            // waiting while write stream will be closed
            out.on('close', function () {
                switch (req.params.file_type) {
                    case 'avatar': // user avatars
                        uploadFile.path_resized = uploadFile.path.split('.')[0] + '_r.' + uploadFile.path.split('.').pop();
                        // resize uploaded image
                        sharp(uploadFile.path)
                            .toFormat('jpeg').toFile(uploadFile.path_resized, function (err) {
                                // remove uploaded file
                                if (fs.existsSync(uploadFile.path)) {
                                    fs.unlinkSync(uploadFile.path);
                                }

                                if(err) {
                                    return res.status(400).send({status: 'Error', errors: [err.message], filename: uploadFile.filename});
                                }

                                if(uploadFile.to_update && fs.existsSync(uploadFile.path_resized)) {
                                    fs.renameSync(uploadFile.path_resized, uploadFile.path);
                                    uploadFile.path_resized = uploadFile.path; // rename resized to file with original filename
                                }

                                //сообщаем что все хорошо
                                return res.send({
                                    status: 'ok',
                                    text: 'Success',
                                    filename: uploadFile.filename,
                                    filepath: uploadFile.path_resized
                                });
                        });
                        break;
                    case 'gallery': // user albums, casting, event, news images, etc.
                        uploadFile.path_resized = uploadFile.path.split('.')[0] + '_r.' + uploadFile.path.split('.').pop();
                        // resize uploaded image
                        sharp(uploadFile.path)
                            .rotate()
                            .resize(1240, 930)
                            .max()
                            .toFormat('jpeg').toFile(uploadFile.path_resized, function (err) {
                                // remove uploaded file
                                if (fs.existsSync(uploadFile.path)) {
                                    fs.unlinkSync(uploadFile.path);
                                }

                                if(err) {
                                    return res.status(400).send({status: 'Error', errors: [err.message], filename: uploadFile.filename});
                                }

                                if(uploadFile.to_update && fs.existsSync(uploadFile.path_resized)) {
                                    fs.renameSync(uploadFile.path_resized, uploadFile.path);
                                    uploadFile.path_resized = uploadFile.path; // rename resized to file with original filename
                                }

                                //сообщаем что все хорошо
                                return res.send({
                                    status: 'ok',
                                    text: 'Success',
                                    filename: uploadFile.filename,
                                    filepath: uploadFile.path_resized
                                });
                        });
                        break;
                    case 'portfolio_video':
                    default:
                        // without resize
                        res.send({status: 'ok', text: 'Success', filename: uploadFile.filename, filepath: uploadFile.path});
                        break;
                }
            });
        } else {
            // skip
            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
            res.status(400).send({status: 'Error', errors: errors, filename: uploadFile.filename});
        }

        part.on('error', function (errors) {
            res.status(400).send({status: 'Error', errors: errors, filename: uploadFile.filename});
        });
    });

    // form parsing
    form.parse(req);

});

app.get('/images/:entity_id/:file_type/:file_name', function (req, res) {
    var filePath = uploadFolderPath[req.params.file_type] + req.params.entity_id + '/' + req.params.file_type + '/' + req.params.file_name;
    if (fs.existsSync(filePath)) {
        var origin = req.headers.origin;
        if (corsOrigins.indexOf(origin) !== -1) {
            res.set('Access-Control-Allow-Origin', origin);
        }
        if (req.param('as_base64') == 'true') {
            var img = fs.readFileSync(filePath).toString('base64');
            var mime = filePath.split('.').pop();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('data:image/' + mime + ';base64,' + img);
        } else {
            var img = fs.readFileSync(filePath);
            res.writeHead(200, {'Content-Type': 'image/' + filePath.split('.').pop()});
            res.end(img, 'binary');
        }
    } else {
        res.status(404).send({status: 'Error', errors: 'File not found'});
    }
});

app.get('/videos/:file_path', function(req, res) {
    var file = 'videos/' + req.params.file_path;

    fs.stat(file, function (err, stats) {
        if (err) {
            if (err.code === 'ENOENT') {
                // 404 Error if file not found
                return res.sendStatus(404);
            }
            res.end(err);
        }
        var range = req.headers.range;
        if (!range) {
            // 416 Wrong range
            return res.sendStatus(416);
        }
        var positions = range.replace(/bytes=/, "").split("-");
        var start = parseInt(positions[0], 10);
        var total = stats.size;
        var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
        var chunksize = (end - start) + 1;

        res.writeHead(206, {
            "Content-Range": "bytes " + start + "-" + end + "/" + total,
            "Accept-Ranges": "bytes",
            "Content-Length": chunksize,
            "Content-Type": "video/mp4"
        });

        var stream = fs.createReadStream(file, {start: start, end: end})
            .on("open", function () {
                stream.pipe(res);
            }).on("error", function (err) {
                res.end(err);
            });
    });
});

app.get('/videos/:entity_id/:file_type/:file_name', function(req, res) {
    var file = 'videos/'+req.params.entity_id+'/'+req.params.file_type+'/'+req.params.file_name;

    fs.stat(file, function(err, stats) {
        if (err) {
            if (err.code === 'ENOENT') {
                // 404 Error if file not found
                return res.sendStatus(404);
            }
            res.end(err);
        }
        var range = req.headers.range;
        if (!range) {
            // 416 Wrong range
            return res.sendStatus(416);
        }
        var positions = range.replace(/bytes=/, "").split("-");
        var start = parseInt(positions[0], 10);
        var total = stats.size;
        var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
        var chunksize = (end - start) + 1;

        res.writeHead(206, {
            "Content-Range": "bytes " + start + "-" + end + "/" + total,
            "Accept-Ranges": "bytes",
            "Content-Length": chunksize,
            "Content-Type": "video/mp4"
        });

        var stream = fs.createReadStream(file, {start: start, end: end})
            .on("open", function () {
                stream.pipe(res);
            }).on("error", function (err) {
                res.end(err);
            });
    });
});

app.delete("/:entity_id/:file_type/:file_name", function (req, res, next) {
    //if (!checkPermission(req)) res.status(403).send({message: 'You are not permitted to perform this action!'});
    if (avlFileTypes.indexOf(req.params.file_type) == -1) return res.status(400).send({
        status: 'Error',
        errors: ['"' + req.params.file_type + '" file type doesn\'t support.']
    });

    var filePath = uploadFolderPath[req.params.file_type] + req.params.entity_id + '/' + req.params.file_type + '/' + req.params.file_name;

    if (fs.existsSync(filePath)) {
        //если загружаемый файл существует удаляем его
        fs.unlinkSync(filePath);
        var files = fs.readdirSync(uploadFolderPath[req.params.file_type] + req.params.entity_id + '/' + req.params.file_type);
        if (!files.length) { // if folder is empty - remove
            fs.rmdirSync(uploadFolderPath[req.params.file_type] + req.params.entity_id + '/' + req.params.file_type);
            var child_folders = fs.readdirSync(uploadFolderPath[req.params.file_type] + req.params.entity_id);
            if (!child_folders.length) { // there are no child folders
                fs.rmdirSync(uploadFolderPath[req.params.file_type] + req.params.entity_id);
                res.send({
                    status: 'ok',
                    text: 'File successfully removed',
                    filename: req.params.file_name,
                    filepath: filePath
                });
            } else {
                res.send({
                    status: 'ok',
                    text: 'File successfully removed',
                    filename: req.params.file_name,
                    filepath: filePath
                });
            }
        } else {
            // folder is not empty - leave it
            res.send({
                status: 'ok',
                text: 'File successfully removed',
                filename: req.params.file_name,
                filepath: filePath
            });
        }
    } else {
        res.status(404).send({status: 'Error', errors: 'File not found.'});
    }
});

/**
 * Clear gallery folder at once
 */
app.delete("/clear_gallery/:entity_id", function (req, res, next) {
    //if (!checkPermission(req)) return res.status(403).send({message: 'You are not permitted to perform this action!'});

    var filePath = uploadFolderPath['gallery'] + req.params.entity_id + '/gallery';
    var shell = require('shelljs');

    shell.rm('-rf', filePath + '/*');

    return res.send({
        status: 'ok',
        text: 'Success'
    })
});
/**
 * Clear folder with portfolio videos
 */
app.delete("/clear_portfolio_videos/:entity_id", function (req, res, next) {
    //if (!checkPermission(req)) return res.status(403).send({message: 'You are not permitted to perform this action!'});

    var filePath = uploadFolderPath['portfolio_video'] + req.params.entity_id + '/portfolio_video';
    var shell = require('shelljs');

    shell.rm('-rf', filePath + '/*');

    return res.send({
        status: 'ok',
        text: 'Success'
    })
});

app.listen(PORT, function (err) {
    if (err) console.error(err);
    console.log('Server is running on http://localhost:' + PORT)
});